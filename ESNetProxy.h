//ESGateProxy.h  
#ifndef ES_NET_PROXY_
#define ES_NET_PROXY_  

#include <vector>
#include <string.h>
#include "ESConn.h"
#include "ESThreadEvent.h"

namespace ES
{
	using namespace std;
	class ESNetProxy : public ESThreadEvent  
	{  
		
		protected:  
			//重载各个处理业务的虚函数  
			void onRead( ESConn *conn);  
			void onWrite( ESConn *conn);  
			void onAccept( ESConn *conn);  
			void onClose( ESConn *conn, short events);  
			void send(ESConn *conn , char *buffer, int len );
		public:  
			ESNetProxy(int count) : ESThreadEvent(count)
			{
			   pthread_mutex_init( &m_connLock, NULL ); 
			}
			
			~ESNetProxy() { }   

			//退出事件，响应Ctrl+C  
			static void onQuit(int sig, short events, void *data);  
			//定时器事件，每10秒向所有客户端发一句hello, world  
			static void onTimeOut(int id, int short events, void *data);
		private:  
			vector<ESConn*> m_vecConn;
			pthread_mutex_t m_connLock; 
	};
}

#endif
