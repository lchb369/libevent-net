//GateThread.h  
#ifndef ES_CONN_QUEUE_H_  
#define ES_CONN_QUEUE_H_  

#include "ESConn.h"
//#include "ESThreadEvent.h"
namespace ES
{
	//这个类一个链表的结点类，结点里存储各个连接的信息，  
	//并提供了读写数据的接口  
	//带头尾结点的双链表类，每个结点存储一个连接的数据  
	struct ESThreadContext;
	class ESConn;
	class ESThreadEvent;
	class ESConnQueue 
	{  
		private:  
			ESConn *m_head;  
			ESConn *m_tail;
			pthread_mutex_t m_lock;  
		public:  
			ESConnQueue();  
			~ESConnQueue();  
			ESConn *insertConn(int fd, ESThreadContext *t);  
			void deleteConn(ESConn *c);  
			//void PrintQueue();  
	}; 

    struct ESThreadContext
    {
        pthread_t tid;              //线程的ID  
        struct event_base *eventBase;    //libevent的事件处理机  
        struct event notifyEvent;   //监听管理的事件机  
        int notifyReceiveFd;        //管道的接收端  
        int notifySendFd;           //管道的发送端  
        ESConnQueue connectQueue;     //socket连接的链表
        ESThreadEvent *tcpConnect;  //TcpBaseServer类的指针  
    };	
 
}
#endif
