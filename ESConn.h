//GateThread.h  
#ifndef ES_CONN_H_  
#define ES_CONN_H_  

#include <event.h>    
#include <event2/bufferevent.h>    
#include <event2/buffer.h>    
#include <event2/listener.h>    
#include <event2/util.h>    
#include <event2/event.h>

#include "ESThreadEvent.h"

namespace ES
{

	//这个类一个链表的结点类，结点里存储各个连接的信息，  
	//并提供了读写数据的接口  
	class ESConn  
	{  
		//此类只能由TcpBaseServer创建，  
		//并由ConnQueue类管理  
		friend class ESConnQueue;  
		friend class ESThreadEvent;  

		private:
			//socket的ID
			const int m_fd;
			//读数据的缓冲区  	
			evbuffer *m_readBuf;
			//写数据的缓冲区
			evbuffer *m_writeBuf;       
			//前一个结点的指针  
			ESConn *m_prev;
			//后一个结点的指针
			ESConn *m_next;
			//IO线程
			ESThreadContext *m_thread;   

			ESConn(int fd=0);  
			~ESConn();  

		public: 
 
			ESThreadContext *getThread()
			{ 
				return m_thread; 
			}  
			
			int getFd()
			{ 
				return m_fd; 
			}  

			//获取可读数据的长度  
			int getReadBufferLen()  
			{ 
				return evbuffer_get_length(m_readBuf); 
			}  

			//从读缓冲区中取出len个字节的数据，存入buffer中，若不够，则读出所有数据  
			//返回读出数据的字节数  
			int getReadBuffer(char *buffer, int len)  
			{ 
				return evbuffer_remove(m_readBuf, buffer, len); 
			}  

			//从读缓冲区中复制出len个字节的数据，存入buffer中，若不够，则复制出所有数据  
			//返回复制出数据的字节数  
			//执行该操作后，数据还会留在缓冲区中，buffer中的数据只是原数据的副本  
			int copyReadBuffer(char *buffer, int len)  
			{ 
				return evbuffer_copyout(m_readBuf, buffer, len); 
			}  

			//获取可写数据的长度  
			int getWriteBufferLen()  
			{ 
				return evbuffer_get_length(m_writeBuf); 
			}  

			//将数据加入写缓冲区，准备发送  
			int addToWriteBuffer(char *buffer, int len)  
			{ 
				return evbuffer_add(m_writeBuf, buffer, len); 
			}  

			//将读缓冲区中的数据移动到写缓冲区  
			void moveBufferData()  
			{ 
				evbuffer_add_buffer(m_writeBuf, m_readBuf); 
			}
	public:
			int send( char *buffer, int len )
			{
				addToWriteBuffer( buffer , len );
			}

	};  
}
#endif
