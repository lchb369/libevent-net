#include <memory.h>
#include "ESRingBuffer.h"

RingBuffer::RingBuffer()
{
	m_data = new char[RING_BUFFER_MAX_SIZE];
	m_readIndex = m_writeIndex = 0;
}

RingBuffer::~RingBuffer()
{
	if (m_data)
	{
		delete []m_data;
	}
}

RingBufferError RingBuffer::pushBack(char* inData, int inSzie)
{
	if (inSzie <= 0)
	{
		return Error_NoError;
	}

	if (inSzie > RING_BUFFER_MAX_SIZE - getSize())
	{
		return Error_NoEnoughSpace;
	}
	int newIdx = m_writeIndex + inSzie;
	if (newIdx <= RING_BUFFER_MAX_SIZE)
	{
		memcpy(&m_data[m_writeIndex], inData, inSzie);
		m_writeIndex = newIdx % RING_BUFFER_MAX_SIZE;
	}
	else
	{
		memcpy(&m_data[m_writeIndex], inData, RING_BUFFER_MAX_SIZE - m_writeIndex);
		memcpy(&m_data[0], &inData[RING_BUFFER_MAX_SIZE - m_writeIndex], inSzie - RING_BUFFER_MAX_SIZE + m_writeIndex);
		m_writeIndex = inSzie - RING_BUFFER_MAX_SIZE + m_writeIndex;
	}
	return Error_NoError;
}

RingBufferError RingBuffer::popFront(char* outData, int inSize)
{
	if (inSize > getSize())
	{
		return Error_NoEnoughData;
	}

	int newIdx = m_readIndex + inSize;
	if (newIdx <= RING_BUFFER_MAX_SIZE)
	{
		memcpy(outData, &m_data[m_readIndex], inSize);
		m_readIndex = newIdx % RING_BUFFER_MAX_SIZE;
	}
	else
	{
		memcpy(outData, &m_data[m_readIndex], RING_BUFFER_MAX_SIZE - m_readIndex);
		memcpy(&outData[RING_BUFFER_MAX_SIZE - m_readIndex], m_data, inSize - RING_BUFFER_MAX_SIZE + m_readIndex);
		m_readIndex = inSize - RING_BUFFER_MAX_SIZE + m_readIndex;
	}

	return Error_NoError;
}

int RingBuffer::getSize()
{
	if (m_readIndex <= m_writeIndex )
	{
		return m_writeIndex - m_readIndex;
	}
	else
	{
		return RING_BUFFER_MAX_SIZE - 1 - m_readIndex + m_writeIndex;
	}
}
