#include "ESNetProxy.h"

namespace ES
{

	void ESNetProxy::onAccept(ESConn *conn)  
	{  
		ESNetProxy *net = (ESNetProxy*)conn->getThread()->tcpConnect;  
		printf("new connection: %d thread:%lu\n", conn->getFd() , pthread_self() );
		net->m_vecConn.push_back(conn);
	}  

	void ESNetProxy::onRead(ESConn *conn)  
	{  
		int len = conn->getReadBufferLen();
        	printf( "onread %lu len=%d \n" , pthread_self() , len );
		
		//如果m_readbuf一次性没有被用完，下次收到数据会连接到后面，不会被覆盖
		conn->moveBufferData();
	}  

	void ESNetProxy::onWrite(ESConn *conn)  
	{  

	}  

	void ESNetProxy::send(ESConn *conn , char *buffer, int len )
	{
		conn->addToWriteBuffer( buffer , len );
	}
	
	void ESNetProxy::onClose(ESConn *conn, short events)  
	{
		ESNetProxy *net = (ESNetProxy*)conn->getThread()->tcpConnect;
		printf("connection closed: %d connSize:%d \n", conn->getFd() , net->m_vecConn.size());
		for( vector<ESConn*>::iterator it = net->m_vecConn.begin() ; it != net->m_vecConn.end() ;  )
		{
			if( *it == conn )
			  it = net->m_vecConn.erase( it );
			else
			  it++;
		}                
	}  

	void ESNetProxy::onQuit(int sig, short events, void *data)  
	{   
		printf("Catch the SIGINT signal, quit in one second\n");  
		ESNetProxy *net = (ESNetProxy*)data;  
		timeval tv = {1, 0};  
		net->stopRun(&tv);  
	}  

	void ESNetProxy::onTimeOut(int id, short events, void *data )
	{  
		ESNetProxy *net = (ESNetProxy*)data;  
		char temp[64];  
		sprintf( temp , "hello thread %lu\n" , pthread_self() );
		for(int i=0; i<net->m_vecConn.size(); i++)
		{ 
			//printf("onTimeOut...\n");
			net->send( net->m_vecConn[i] , temp, strlen(temp) );
			//net->m_vecConn[i]->addToWriteBuffer(temp, strlen(temp));  
		}
	}  
}
