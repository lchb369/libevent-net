
cmake_minimum_required (VERSION 2.6)
project ( server )

include_directories("../../")

SET (HEADER_FILES
        ../ESConn.h
        ../ESConnQueue.h
        ../ESNetProxy.h
        ../ESThreadEvent.h
        ../ESServer.h
)

SET (SOURCE_FILES
        ../ESConn.cpp
        ../ESConnQueue.cpp
        ../ESNetProxy.cpp
        ../ESThreadEvent.cpp
        ../ESServer.cpp
)


ADD_EXECUTABLE( server
 ${HEADER_FILES}  ${SOURCE_FILES}
)

TARGET_LINK_LIBRARIES( server pthread event )                                                 
