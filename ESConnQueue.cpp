//ESConnQueue.cpp  
#include "ESConnQueue.h"

namespace ES
{
	ESConnQueue::ESConnQueue()  
	{  
		pthread_mutex_init( &m_lock, NULL );
		//建立头尾结点，并调整其指针  
		m_head = new ESConn(0);  
		m_tail = new ESConn(0);  
		m_head->m_prev = m_tail->m_next = NULL;  
		m_head->m_next = m_tail;  
		m_tail->m_prev = m_head;  
	}  

	ESConnQueue::~ESConnQueue()  
	{  
		ESConn *tcur, *tnext;  
		tcur = m_head;  
		//循环删除链表中的各个结点  
		while( tcur != NULL )  
		{  
			tnext = tcur->m_next;  
			delete tcur;  
			tcur = tnext;  
		}  
	}  

	ESConn *ESConnQueue::insertConn(int fd, ESThreadContext *t)  
	{  
		ESConn *c = new ESConn(fd);  
		c->m_thread = t;  
		ESConn *next = m_head->m_next;  

		c->m_prev = m_head;  
		c->m_next = m_head->m_next;  
		
		pthread_mutex_lock( &m_lock );
		m_head->m_next = c;  
		next->m_prev = c;
		pthread_mutex_unlock( &m_lock );
		return c;  
	}  

	void ESConnQueue::deleteConn(ESConn *c)  
	{ 
		pthread_mutex_lock( &m_lock ); 
		c->m_prev->m_next = c->m_next;  
		c->m_next->m_prev = c->m_prev;
		pthread_mutex_unlock( &m_lock );  
		delete c;  
	}  

	/* 
	void ESConnQueue::PrintQueue() 
	{ 
	ESConn *cur = m_head->m_Next; 
	while( cur->m_Next != NULL ) 
	{ 
	printf("%d ", cur->m_fd); 
	cur = cur->m_Next; 
	} 
	printf("\n"); 
	} 
	*/  
}
