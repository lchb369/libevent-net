
//ESThreadEvent.h  
#ifndef ES_THREAD_EVENT_H_  
#define ES_THREAD_EVENT_H_

#include <stdio.h>    
#include <stdlib.h>    
#include <unistd.h>    
#include <string.h>    
#include <errno.h>    
#include <signal.h>    
#include <time.h>    
#include <pthread.h>    
#include <fcntl.h>    
#include <assert.h> 

#include "ESConn.h"
#include "ESConnQueue.h"

namespace ES
{
	class  ESConn;
    	struct ESThreadContext;	

	class ESThreadEvent
	{  
		private:
			static const int EXIT_CODE = -1;  
			static const int MAX_SIGNAL = 256;

		private:  
			int m_threadCount;                  //子线程数  
			int m_port;                         //监听的端口  
			ESThreadContext *m_mainThread;        //主线程的libevent事件处理机  
			ESThreadContext *m_workerThreads;   //存储各个子线程信息的数组  
			event *m_signalEvents[MAX_SIGNAL];	//自定义的信号处理  
		        static pthread_mutex_t m_connLock;	
		private:  
			//初始化子线程的数据  
			void initWorkerThread( ESThreadContext *thread );  

			//子线程的入门函数  
			static void *workerLibevent(void *arg);  
			//（主线程收到请求后），对应子线程的处理函数  
			static void threadProcess(int fd, short which, void *arg);  
			//被libevent回调的各个静态函数  
			static void listenerEventCb( evconnlistener *listener, evutil_socket_t fd,  
				sockaddr *sa, int socklen, void *user_data);  
			static void readEventCb(struct bufferevent *bev, void *data);  
			static void writeEventCb(struct bufferevent *bev, void *data);   
			static void closeEventCb(struct bufferevent *bev, short events, void *data);  

		protected:  
			//这五个虚函数，一般是要被子类继承，并在其中处理具体业务的  

			//新建连接成功后，会调用该函数  
			virtual void onAccept(ESConn *conn) { }  

			//读取完数据后，会调用该函数  
			virtual void onRead(ESConn *conn) { }  

			//发送完成功后，会调用该函数（因为串包的问题，所以并不是每次发送完数据都会被调用）  
			virtual void onWrite(ESConn *conn) { }  

			//断开连接（客户自动断开或异常断开）后，会调用该函数  
			virtual void onClose(ESConn *conn, short events) { }  

		public:  
			ESThreadEvent(int count);  
			~ESThreadEvent();  

			//设置监听的端口号，如果不需要监听，请将其设置为EXIT_CODE  
			void setPort(int port)  
			{ m_port = port; }  

			//开始事件循环  
			bool startRun(); 
			
			//在tv时间里结束事件循环  
			//否tv为空，则立即停止  
			void stopRun(timeval *tv);  

			//添加和删除信号处理事件  
			//sig是信号，ptr为要回调的函数  
			bool addSignalEvent(int sig, void (*ptr)(int, short, void*));  
			bool deleteSignalEvent(int sig);  

			//添加和删除定时事件  
			//ptr为要回调的函数，tv是间隔时间，once决定是否只执行一次  
			event *addTimerEvent(void(*ptr)(int, short, void*),  
				timeval tv, bool once);  
			bool deleteTimerEvent(event *ev);  
	};  
}
#endif  
