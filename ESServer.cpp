
#include "ESServer.h"
using namespace ES;
int main()  
{  
	printf("pid: %d thread:%lu \n", getpid() , pthread_self() );
	ESNetProxy netServer( 3 );
	netServer.addSignalEvent( SIGINT, ESNetProxy::onQuit );  
	timeval tv = {10, 0};  
	netServer.addTimerEvent( ESNetProxy::onTimeOut, tv, false);  
	netServer.setPort( 3456 );  
	netServer.startRun();
	printf("done\n");
	return 0;  
}

