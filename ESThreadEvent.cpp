//ESThreadEvent.h  
#include "ESConn.h"
#include "ESThreadEvent.h"  
#include <iostream>
namespace ES
{
	pthread_mutex_t ESThreadEvent::m_connLock;	

	ESThreadEvent::ESThreadEvent(int count)  
	{  
		//初始化各项数据  
		m_threadCount = count;  
		m_port = -1;  
		m_mainThread = new ESThreadContext;  
		m_workerThreads = new ESThreadContext[m_threadCount];  
		m_mainThread->tid = pthread_self();  
		m_mainThread->eventBase = event_base_new();
		memset(m_signalEvents, 0, sizeof(m_signalEvents));
		pthread_mutex_init( &m_connLock, NULL );

		//初始化各个子线程的结构体  
		for(int i=0; i<m_threadCount; i++)  
		{  
			initWorkerThread( &m_workerThreads[i] );  
		}  

	}  

	ESThreadEvent::~ESThreadEvent()  
	{  
		//停止事件循环（如果事件循环没开始，则没效果）  
		stopRun(NULL);  

		//释放内存  
		event_base_free( m_mainThread->eventBase );  
		for(int i=0; i<m_threadCount; i++)  
			event_base_free( m_workerThreads[i].eventBase );  

		delete m_mainThread;  
		delete [] m_workerThreads;  
	}  

	void ESThreadEvent::initWorkerThread( ESThreadContext *worker )  
	{  
		int res;
		//建立libevent事件处理机制  
		worker->tcpConnect = this;  
		worker->eventBase = event_base_new();  
		assert( worker->eventBase != NULL );

		//在主线程和子线程之间建立管道  
		int fds[2];  
		res = pipe(fds);  
		assert( res == 0 );
		worker->notifyReceiveFd = fds[0];  
		worker->notifySendFd = fds[1];  

		//让子线程的状态机监听管道  
		event_set( &worker->notifyEvent, worker->notifyReceiveFd,  EV_READ | EV_PERSIST, threadProcess, worker );  
		event_base_set( worker->eventBase, &worker->notifyEvent );  
		res = event_add( &worker->notifyEvent, 0 );
		assert( res == 0 );
	}

	void *ESThreadEvent::workerLibevent(void *arg)  
	{  
		//开启libevent的事件循环，准备处理业务  
		ESThreadContext *thread = (ESThreadContext*)arg;  
		printf("worker thread %lu started\n", thread->tid );  
		event_base_dispatch( thread->eventBase );  
		//printf("subthread done\n");  
	}

	bool ESThreadEvent::startRun()  
	{  
		evconnlistener *listener;
		//如果端口号不是EXIT_CODE，就监听该端口号  
		if( m_port != EXIT_CODE )  
		{  
			sockaddr_in sin;  
			memset(&sin, 0, sizeof(sin));  
			sin.sin_family = AF_INET;  
			sin.sin_port = htons(m_port);  
			//主线程监听套接字
			listener = evconnlistener_new_bind( m_mainThread->eventBase,   
				listenerEventCb, (void*)this,  
				LEV_OPT_REUSEABLE|LEV_OPT_CLOSE_ON_FREE, -1,  
				(sockaddr*)&sin, sizeof(sockaddr_in));  
			if( NULL == listener )
			{
				fprintf(stderr, "listen error: %s\n", strerror(errno));
				exit(1);
			}
		}  

		//开启各个worker子线程做io操作
		for(int i=0; i<m_threadCount; i++)  
		{  
			pthread_create( &m_workerThreads[i].tid, NULL,  workerLibevent, (void*)&m_workerThreads[i] );
		}  

		//开启主线程的事件循环  
		event_base_dispatch( m_mainThread->eventBase );  

		//事件循环结果，释放监听者的内存  
		if( m_port != EXIT_CODE )  
		{  
			//printf("free listen\n");  
			evconnlistener_free( listener );  
		}  
	}  

	void ESThreadEvent::stopRun(timeval *tv)  
	{  
		int contant = EXIT_CODE;  
		//向各个子线程的管理中写入EXIT_CODE，通知它们退出  
		for(int i=0; i<m_threadCount; i++)  
		{  
			write( m_workerThreads[i].notifySendFd, &contant, sizeof(int));  
		}  
		//结果主线程的事件循环  
		event_base_loopexit( m_mainThread->eventBase, tv);  
	}  

	void ESThreadEvent::listenerEventCb(
		struct evconnlistener *listener,   
		evutil_socket_t fd,  
		struct sockaddr *sa,   
		int socklen,   
		void *user_data)  
	{  
		ESThreadEvent *threadEvent = (ESThreadEvent*)user_data;  

		//随机选择一个子线程，通过管道向其传递socket描述符  
		int num = rand() % threadEvent->m_threadCount;  
		int sendfd = threadEvent->m_workerThreads[num].notifySendFd;  
		write( sendfd, &fd, sizeof(evutil_socket_t ));  
	}  

	void ESThreadEvent::threadProcess(int fd, short which, void *arg)  
	{  
		ESThreadContext *threadContext = (ESThreadContext*)arg;  

		//从管道中读取数据（socket的描述符或操作码）  
		int pipefd = threadContext->notifyReceiveFd;  
		evutil_socket_t confd;  
		read(pipefd, &confd, sizeof(evutil_socket_t));  

		//如果操作码是EXIT_CODE，则终于事件循环  
		if( EXIT_CODE == confd )  
		{  
			event_base_loopbreak( threadContext->eventBase );  
			return;  
		}  

		pthread_mutex_lock( &m_connLock );	
		//新建连接  
		struct bufferevent *bev;  
		bev = bufferevent_socket_new( threadContext->eventBase, confd, BEV_OPT_CLOSE_ON_FREE );
		if (!bev)  
		{  
			fprintf(stderr, "Error constructing bufferevent!");  
			event_base_loopbreak( threadContext->eventBase );  
			return;  
		}

		//将该链接放入队列  
		ESConn *conn = threadContext->connectQueue.insertConn( confd, threadContext );  

		//准备从socket中读写数据  
		bufferevent_setcb(bev, readEventCb, writeEventCb, closeEventCb, conn);  
		bufferevent_enable(bev, EV_WRITE);  
		bufferevent_enable(bev, EV_READ);  

		//调用用户自定义的连接事件处理函数  
		threadContext->tcpConnect->onAccept(conn);  
		pthread_mutex_unlock( &m_connLock );
	}  

	void ESThreadEvent::readEventCb(struct bufferevent *bev, void *data)  
	{  
		ESConn *conn = (ESConn*)data;  
		conn->m_readBuf = bufferevent_get_input(bev);  
		conn->m_writeBuf = bufferevent_get_output(bev);  

		//调用用户自定义的读取事件处理函数  
		conn->m_thread->tcpConnect->onRead(conn);  
	}   

	void ESThreadEvent::writeEventCb(struct bufferevent *bev, void *data)  
	{  
		ESConn *conn = (ESConn*)data;  
		conn->m_readBuf = bufferevent_get_input(bev);  
		conn->m_writeBuf = bufferevent_get_output(bev);  

		//调用用户自定义的写入事件处理函数  
		conn->m_thread->tcpConnect->onWrite(conn);  

	}  

	void ESThreadEvent::closeEventCb(struct bufferevent *bev, short events, void *data)  
	{ 
		pthread_mutex_lock( &m_connLock ); 
		ESConn *conn = (ESConn*)data;  
		//调用用户自定义的断开事件处理函数  
		conn->m_thread->tcpConnect->onClose(conn, events);  
		conn->getThread()->connectQueue.deleteConn(conn);  
		bufferevent_free(bev);  
		pthread_mutex_unlock( &m_connLock );
	}  

	bool ESThreadEvent::addSignalEvent(int sig, void (*ptr)(int, short, void*))  
	{  
		if( sig >= MAX_SIGNAL )
			return false;

		//新建一个信号事件  
		event *ev = evsignal_new(m_mainThread->eventBase, sig, ptr, (void*)this);  
		if ( !ev ||   
			event_add(ev, NULL) < 0 )  
		{  
			event_del(ev);  
			return false;  
		}  

		//删除旧的信号事件（同一个信号只能有一个信号事件） 
		if( NULL != m_signalEvents[sig] )
			deleteSignalEvent(sig);  
		m_signalEvents[sig] = ev;  

		return true;  
	}  

	bool ESThreadEvent::deleteSignalEvent(int sig)  
	{  
		event *ev = m_signalEvents[sig];
		if( sig >= MAX_SIGNAL || NULL == ev )
			return false;

		event_del(ev);  
		ev = NULL;
		return true;  
	}  

	event *ESThreadEvent::addTimerEvent( void (*ptr)(int, short, void *),   
										 timeval tv, bool once )  
	{  
		int flag = 0;  
		if( !once )  
			flag = EV_PERSIST;  

		//新建定时器信号事件  
		event *ev = new event;  
		event_assign(ev, m_mainThread->eventBase, -1, flag, ptr, (void*)this);  
		if( event_add(ev, &tv) < 0 )  
		{  
			event_del(ev);  
			return NULL;  
		}  
		return ev;  
	}  

	bool ESThreadEvent::deleteTimerEvent(event *ev)  
	{  
		int res = event_del(ev);  
		return (0 == res);  
	}  
}
