#ifndef _RING_BUFFER_H_
#define _RING_BUFFER_H_

#define RING_BUFFER_MAX_SIZE 65535

enum RingBufferError
{
	Error_NoError,
	Error_NoEnoughSpace,
	Error_NoEnoughData,
};

class RingBuffer
{
	private:
	char* m_data;
	int m_readIndex;
	int m_writeIndex;

	public:
	RingBuffer();
	~RingBuffer();
	RingBufferError pushBack(char* inData, int inSzie);
	RingBufferError popFront(char* outData, int inSize);

	int getSize();
};


#endif
